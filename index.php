<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<title>Amazon, Inc. Trading</title>
	<link rel="icon" type="image/x-icon" href="images/favicon.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="font/stylesheet.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-M7LW664');
	</script>
	<!-- End Google Tag Manager -->
</head>

<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="" https://www.googletagmanager.com/ns.html?id=GTM-M7LW664"" height="" 0"" width="" 0"" style="" display:none;visibility:hidden""></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<?php if (isset($_GET['error'])) {
		$error = $_GET['error']; ?>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#server_error_msg").show();
				window.setTimeout(function() {
					$(".alert").fadeTo(1000, 0).slideUp(1000, function() {
						$(this).remove();
					});
				}, 4000);
			})
		</script>
	<?php } else {
		$error = ""; ?>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#server_error_msg").hide();
			})
		</script>
	<?php } ?>

	<header id="top">
		<div class="row logo-header">
			<div class="col-md-12 d-flex">
				<img class="logo" src="images/logo.png">
				<button type="button" class="start-btn" style="display:none;">Start Trading</button>
				<div id="close-btn" style="display:none;"> X </div>
			</div>
		</div>
	</header>

	<section class="sec-frst">
		<div class="alert alert-danger" role="alert" id="server_error_msg" style="display:none; width:80%; margin:0 auto;">
			<?php echo $error; ?>
		</div>
		<div class="row">
			<div class="col-md-6 text-column">
				<div class="heading">
					<h2>Should You Invest In <b>Amazon, Inc.?</b></h2>
					<p class="desc"> Get the facts about trading Amazon, Inc. before you start. Discuss investment strategies, review market research, and get real-time updates </p>
					<div class="bottom-green"></div>
					<div class="amz-buttons">
						<button type="button" class="frst-btn">Buy AMZ</button>
						<button type="button" class="scnd-btn">Sell AMZ</button>
						<button type="button" class="read-btn">Read More</button>
					</div>
				</div>
			</div>
			<div class="col-md-5 phn-img">
				<div class="phn-img-div">
					<img src="images/phone.gif" class="phn-img">
				</div>
			</div>
		</div>
	</section>
	<div class="sect-divider"></div>

	<div class="section sec-scnd wrapper">
		<div class="investing">
			<h2>Your Opportunity To become a stakeholder of Amazon, Google, Apple and Facebook </h2>
			<p>Amazon has several opportunities to yield significant and consistent global revenue growth and rising profit margins over the next few years, a Wall street analyst said Tuesday, as he named the e-commerce giant one of his “Best Investment Ideas” for 2022.</p>
			<p>The E-commerce and cloud computing industries, have started growing exponentially during the end of 2019, and have not yet reached even 5% of their potential, as more people around the world use smartphones, and make their purchases online instead of at normal stores. Amazon is considered to be one of the leaders in both industries, which is why top wallstreet analysts foresee a strong revenue growth for Amazon this upcoming 2022.</p>
		</div>
		<div class="amz-buttons">
			<button type="button" class="frst-btn">Buy AMZ Now</button>
			<button type="button" class="scnd-btn">Sell AMZ Now</button>
		</div>
		<div class="link-scroll"><a class="link">Still not sure if you keep invest? Read more below</a></div>
	</div>
	<div class="sect-divider"></div>

	<div class="section sec-thrd">
		<div class="wrapper">
			<h2>Study the Historical Performance of the Amazon Share </h2>
			<div class="gray-divider"></div>
			<div class="history-img">
				<!-- <img class="historical-chart" src="images/section-1-img.png"> -->
				<!-- TradingView Widget BEGIN -->
				<div class="tradingview-widget-container">
					<div id="tradingview_af741"></div>
					<div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/symbols/NASDAQ-AMZN/" rel="noopener" target="_blank"><span class="blue-text">AMZN Stock Price Today</span></a> by TradingView</div>
					<script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
					<script type="text/javascript">
					new TradingView.MediumWidget(
					{
					"symbols": [
						[
						"NASDAQ:AMZN|12M"
						]
					],
					"chartOnly": false,
					"width": "100%",
					"height": "400",
					"locale": "en",
					"colorTheme": "light",
					"gridLineColor": "rgba(240, 243, 250, 0)",
					"fontColor": "#787B86",
					"isTransparent": false,
					"autosize": true,
					"showFloatingTooltip": true,
					"scalePosition": "no",
					"scaleMode": "Normal",
					"fontFamily": "Trebuchet MS, sans-serif",
					"noTimeScale": false,
					"chartType": "area",
					"lineColor": "#2962FF",
					"bottomColor": "rgba(41, 98, 255, 0)",
					"topColor": "rgba(41, 98, 255, 0.3)",
					"container_id": "tradingview_af741"
					}
					);
					</script>
				</div>
				<!-- TradingView Widget END -->
			</div>
		</div>
	</div>
	<div class="sect-divider"></div>

	<div class="section sec-forth">
		<div class="wrapper">
			<center>
				<h2>Discover the benefits of PhoenixMarkets </h2>
				<p class="p-desc">Join PHOENIXMARKETS to trade with the most professional financial experts, on the most advanced trading platforms. </p>
			</center>
		</div>
		<div class="wrapper">
			<ul class="interest-steps">
				<li class="interest-step">
					<div class="interest-step-head flex-centered">
						<svg viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M37.5 50C37.2917 50 37.0833 49.9586 36.8875 49.8757C36.4042 49.6706 25 44.7323 25 34.5759V26.2527C25 25.5878 25.425 24.9974 26.0604 24.782L36.9979 21.0823C37.3229 20.9726 37.6771 20.9726 38.0021 21.0823L48.9396 24.782C49.575 24.9974 50 25.5878 50 26.2527V34.5759C50 44.7323 38.5958 49.6706 38.1125 49.8778C37.9167 49.9586 37.7083 50 37.5 50ZM28.125 27.3651V34.5738C28.125 41.478 35.325 45.6292 37.5 46.7229C39.675 45.6292 46.875 41.478 46.875 34.5738V27.3651L37.5 24.1937L28.125 27.3651Z" fill="#C9AB63"></path>
							<path d="M36.9232 41C36.4967 41 36.0831 40.8298 35.7815 40.52L31.4734 36.1563C30.8422 35.517 30.8422 34.4806 31.4734 33.8414C32.1045 33.2021 33.1277 33.2021 33.7589 33.8414L36.7509 36.8719L42.093 29.6544C42.623 28.9322 43.6397 28.7838 44.3548 29.3293C45.0679 29.8704 45.2122 30.8959 44.6758 31.6202L38.2135 40.3476C37.9335 40.7273 37.5048 40.9629 37.0352 40.9978C36.9986 40.9978 36.962 41 36.9232 41Z" fill="#C9AB63"></path>
							<path d="M22.8029 44H6.66177C3.53853 44 1 41.4166 1 38.2381V5.76191C1 2.58343 3.53853 0 6.66177 0H30.3382C33.4615 0 36 2.58343 36 5.76191V17.139C36 18.0065 35.3082 18.7105 34.4559 18.7105C33.6035 18.7105 32.9118 18.0065 32.9118 17.139V5.76191C32.9118 4.31829 31.7568 3.14286 30.3382 3.14286H6.66177C5.24324 3.14286 4.08824 4.31829 4.08824 5.76191V38.2381C4.08824 39.6817 5.24324 40.8571 6.66177 40.8571H22.8029C23.6553 40.8571 24.3471 41.5611 24.3471 42.4286C24.3471 43.296 23.6553 44 22.8029 44Z" fill="#C9AB63"></path>
							<path d="M27.4318 20H7.56818C6.70255 20 6 19.328 6 18.5C6 17.672 6.70255 17 7.56818 17H27.4318C28.2975 17 29 17.672 29 18.5C29 19.328 28.2975 20 27.4318 20Z" fill="#C9AB63"></path>
							<path d="M20.3929 28H8.60714C7.72 28 7 27.328 7 26.5C7 25.672 7.72 25 8.60714 25H20.3929C21.28 25 22 25.672 22 26.5C22 27.328 21.28 28 20.3929 28Z" fill="#C9AB63"></path>
							<path d="M17.375 11H7.625C6.728 11 6 10.328 6 9.5C6 8.672 6.728 8 7.625 8H17.375C18.272 8 19 8.672 19 9.5C19 10.328 18.272 11 17.375 11Z" fill="#C9AB63"></path>
						</svg>
					</div>
					<div class="interest-step-body">
						<p class="interest-step-description">
							Balance protection <br>&amp; enhanced insurance
						</p>
					</div>
				</li>
				<li class="interest-step">
					<div class="interest-step-head flex-centered">
						<img class="gif-img" src="images/2.gif" alt="Safe, secure &amp; regulated trading environment">
					</div>
					<div class="interest-step-body">
						<p class="interest-step-description">
							Safe, secure &amp; regulated<br>trading environment
						</p>
					</div>
				</li>
				<li class="interest-step">
					<div class="interest-step-head flex-centered">
						<svg viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M2.59161 27.001C3.28141 27.0021 3.94327 26.7358 4.43102 26.261C4.49863 26.1952 8.6594 22.1526 8.6594 22.1526L10.7944 24.2325C11.6073 25.0245 12.7103 25.4695 13.8604 25.4695C15.0105 25.4695 16.1134 25.0245 16.9263 24.2325L29.6444 11.8484L33.5122 15.6151C35.2066 17.2616 37.9506 17.2616 39.645 15.6151L47.3875 8.06833L48.0307 8.69441C48.6035 9.25142 49.4571 9.43274 50.2162 9.15868C50.9753 8.88461 51.501 8.20534 51.5621 7.41947L51.9955 1.98471V1.97627C52.0331 1.44344 51.8321 0.920832 51.4443 0.543016C51.0565 0.1652 50.5198 -0.0309002 49.9724 0.00521119L44.3839 0.427099C43.5768 0.483287 42.8778 0.993012 42.5954 1.73116C42.3131 2.4693 42.4996 3.29993 43.0724 3.8562L43.7156 4.48397L36.5773 11.4358L32.7104 7.66923C31.8975 6.87728 30.7945 6.43231 29.6444 6.43231C28.4943 6.43231 27.3914 6.87728 26.5784 7.66923L13.8604 20.0542L11.7271 17.9726C10.0329 16.3256 7.28846 16.3256 5.5943 17.9726L0.761737 22.6893C0.0190585 23.4106 -0.204013 24.4964 0.196565 25.4403C0.597144 26.3842 1.54246 27.0002 2.59161 27.001ZM1.98483 23.879L6.82086 19.1615C7.30862 18.6861 7.97051 18.419 8.6607 18.419C9.35089 18.419 10.0128 18.6861 10.5005 19.1615L13.2475 21.8371C13.4101 21.9955 13.6307 22.0845 13.8608 22.0845C14.0909 22.0845 14.3115 21.9955 14.4741 21.8371L27.8041 8.85979C28.8337 7.90212 30.4551 7.90212 31.4847 8.85979L35.3525 12.6264C36.0386 13.2643 37.1186 13.2643 37.8047 12.6264L45.555 5.07799C45.8928 4.74859 45.8928 4.21513 45.555 3.88574L44.2955 2.65973C44.2003 2.57369 44.1702 2.439 44.2201 2.32222C44.2638 2.19816 44.3811 2.1128 44.5157 2.10706L50.1007 1.6877C50.1443 1.68578 50.1867 1.70241 50.2168 1.73326C50.2511 1.76528 50.2695 1.81024 50.2671 1.85646L49.8337 7.29037C49.8242 7.41909 49.7385 7.53061 49.6143 7.57581C49.4901 7.621 49.3503 7.59157 49.2564 7.50047L48.0003 6.27615C47.6562 5.95952 47.1179 5.95952 46.7738 6.27615L38.4184 14.4211C37.3881 15.3761 35.7691 15.3761 34.7388 14.4211L30.871 10.6545C30.5465 10.3365 30.1048 10.1582 29.6444 10.1592C29.1848 10.1587 28.7439 10.3367 28.4196 10.6537L15.7015 23.0394C14.6717 23.9967 13.0507 23.9967 12.021 23.0394L9.88596 20.9595C9.20906 20.3013 8.11233 20.3013 7.43544 20.9595L3.20272 25.0729C2.86309 25.3965 2.32012 25.3965 1.9805 25.0729C1.81654 24.9179 1.72403 24.7048 1.72403 24.4823C1.72403 24.2598 1.81654 24.0467 1.9805 23.8917L1.98483 23.879Z" fill="#C9AB63"></path>
							<path d="M34 32.7618C34.0075 28.6441 31.2217 25.079 27.3113 24.2017C23.4008 23.3243 19.4253 25.3726 17.7658 29.1196C16.1062 32.8667 17.2124 37.2972 20.4218 39.7575H20.4005C19.3928 39.7529 18.4778 40.362 18.0689 41.3096C17.6599 42.2571 17.8345 43.3638 18.5136 44.1298C17.6296 45.1184 17.6296 46.6391 18.5136 47.6277C17.8345 48.3936 17.6599 49.5004 18.0689 50.4479C18.4778 51.3955 19.3928 52.0046 20.4005 52H30.6001C31.6079 52.0046 32.5228 51.3955 32.9318 50.4479C33.3407 49.5004 33.1662 48.3936 32.487 47.6277C33.3711 46.6391 33.3711 45.1184 32.487 44.1298C33.1662 43.3638 33.3407 42.2571 32.9318 41.3096C32.5228 40.362 31.6079 39.7529 30.6001 39.7575H30.5789C32.729 38.1123 33.9974 35.5187 34 32.7618ZM31.4501 45.8787C31.4501 46.3617 31.0695 46.7532 30.6001 46.7532H20.4005C19.9311 46.7532 19.5506 46.3617 19.5506 45.8787C19.5506 45.3958 19.9311 45.0043 20.4005 45.0043H30.6001C31.0695 45.0043 31.4501 45.3958 31.4501 45.8787ZM30.6001 50.2511H20.4005C19.9311 50.2511 19.5506 49.8595 19.5506 49.3766C19.5506 48.8936 19.9311 48.5021 20.4005 48.5021H30.6001C31.0695 48.5021 31.4501 48.8936 31.4501 49.3766C31.4501 49.8595 31.0695 50.2511 30.6001 50.2511ZM30.6001 41.5064C31.0695 41.5064 31.4501 41.8979 31.4501 42.3809C31.4501 42.8639 31.0695 43.2554 30.6001 43.2554H20.4005C19.9311 43.2554 19.5506 42.8639 19.5506 42.3809C19.5506 41.8979 19.9311 41.5064 20.4005 41.5064H30.6001ZM18.7006 32.7618C18.7006 28.8982 21.7449 25.7661 25.5003 25.7661C29.2557 25.7661 32.3001 28.8982 32.3001 32.7618C32.3001 36.6254 29.2557 39.7575 25.5003 39.7575C21.7467 39.7532 18.7048 36.6236 18.7006 32.7618Z" fill="#C9AB63"></path>
							<path fill-rule="evenodd" clip-rule="evenodd" d="M25.4992 31.75C25.0948 31.7496 24.7491 31.4881 24.6792 31.1298C24.6092 30.7714 24.8352 30.4195 25.2152 30.2951C25.5952 30.1708 26.0195 30.3099 26.2217 30.625C26.3656 30.8657 26.6436 31.0169 26.9473 31.0196C27.2511 31.0222 27.5323 30.876 27.6815 30.6379C27.8306 30.3997 27.824 30.1075 27.6642 29.875C27.3621 29.4187 26.8904 29.0734 26.3325 28.9V28.75C26.3325 28.3358 25.9594 28 25.4992 28C25.0389 28 24.6658 28.3358 24.6658 28.75V28.888C23.5187 29.2516 22.8302 30.3069 23.0363 31.3856C23.2424 32.4644 24.2829 33.2521 25.4992 33.25C25.9035 33.2504 26.2492 33.5119 26.3191 33.8702C26.3891 34.2286 26.1631 34.5805 25.7831 34.7049C25.4031 34.8292 24.9788 34.6901 24.7767 34.375C24.5397 34.0302 24.0412 33.9174 23.6511 34.1202C23.2611 34.323 23.1207 34.768 23.3341 35.125C23.6367 35.5815 24.109 35.927 24.6675 36.1V36.25C24.6675 36.6642 25.0406 37 25.5008 37C25.9611 37 26.3342 36.6642 26.3342 36.25V36.112C27.4816 35.7483 28.1701 34.6926 27.9636 33.6136C27.757 32.5347 26.7157 31.7472 25.4992 31.75Z" fill="#C9AB63"></path>
							<path fill-rule="evenodd" clip-rule="evenodd" d="M8.49916 38.75C8.09485 38.7496 7.74915 38.4881 7.6792 38.1298C7.60925 37.7714 7.83523 37.4195 8.21521 37.2951C8.59518 37.1708 9.01951 37.3099 9.22167 37.625C9.3656 37.8657 9.64364 38.0169 9.94734 38.0196C10.2511 38.0222 10.5323 37.876 10.6815 37.6379C10.8306 37.3997 10.824 37.1075 10.6642 36.875C10.3621 36.4187 9.89036 36.0734 9.33251 35.9V35.75C9.33251 35.3358 8.9594 35 8.49916 35C8.03892 35 7.66582 35.3358 7.66582 35.75V35.888C6.51868 36.2516 5.83021 37.3069 6.03631 38.3856C6.2424 39.4644 7.28295 40.2521 8.49916 40.25C8.90348 40.2504 9.24918 40.5119 9.31913 40.8702C9.38908 41.2286 9.16309 41.5805 8.78312 41.7049C8.40314 41.8292 7.97881 41.6901 7.77665 41.375C7.53966 41.0302 7.04115 40.9174 6.6511 41.1202C6.26106 41.323 6.12067 41.768 6.33413 42.125C6.63666 42.5815 7.10902 42.927 7.66748 43.1V43.25C7.66748 43.6642 8.04059 44 8.50083 44C8.96107 44 9.33417 43.6642 9.33417 43.25V43.112C10.4816 42.7483 11.1701 41.6926 10.9636 40.6136C10.757 39.5347 9.71569 38.7472 8.49916 38.75Z" fill="#C9AB63"></path>
							<path fill-rule="evenodd" clip-rule="evenodd" d="M43.4992 25.75C43.0948 25.7496 42.7491 25.4881 42.6792 25.1298C42.6092 24.7714 42.8352 24.4195 43.2152 24.2951C43.5952 24.1708 44.0195 24.3099 44.2217 24.625C44.3656 24.8657 44.6436 25.0169 44.9473 25.0196C45.2511 25.0222 45.5323 24.876 45.6815 24.6379C45.8306 24.3997 45.824 24.1075 45.6642 23.875C45.3621 23.4187 44.8904 23.0734 44.3325 22.9V22.75C44.3325 22.3358 43.9594 22 43.4992 22C43.0389 22 42.6658 22.3358 42.6658 22.75V22.888C41.5187 23.2516 40.8302 24.3069 41.0363 25.3856C41.2424 26.4644 42.2829 27.2521 43.4992 27.25C43.9035 27.2504 44.2492 27.5119 44.3191 27.8702C44.3891 28.2286 44.1631 28.5805 43.7831 28.7049C43.4031 28.8292 42.9788 28.6901 42.7767 28.375C42.5397 28.0302 42.0412 27.9174 41.6511 28.1202C41.2611 28.323 41.1207 28.768 41.3341 29.125C41.6367 29.5815 42.109 29.927 42.6675 30.1V30.25C42.6675 30.6642 43.0406 31 43.5008 31C43.9611 31 44.3342 30.6642 44.3342 30.25V30.112C45.4816 29.7483 46.1701 28.6926 45.9636 27.6136C45.757 26.5347 44.7157 25.7472 43.4992 25.75Z" fill="#C9AB63"></path>
							<path d="M43.4996 18C39.838 17.9958 36.5864 20.4088 35.4329 23.9862C34.2793 27.5636 35.4838 31.4993 38.421 33.75H38.3997C37.392 33.7454 36.477 34.3548 36.068 35.303C35.6591 36.2511 35.8336 37.3585 36.5128 38.125C35.6287 39.1142 35.6287 40.6358 36.5128 41.625C35.6287 42.6142 35.6287 44.1358 36.5128 45.125C35.6287 46.1142 35.6287 47.6358 36.5128 48.625C35.8336 49.3914 35.6591 50.4988 36.068 51.447C36.477 52.3951 37.392 53.0046 38.3997 53H48.5994C49.6072 53.0046 50.5222 52.3951 50.9311 51.447C51.34 50.4988 51.1655 49.3914 50.4864 48.625C51.3704 47.6358 51.3704 46.1142 50.4864 45.125C51.3704 44.1358 51.3704 42.6142 50.4864 41.625C51.3704 40.6358 51.3704 39.1142 50.4864 38.125C51.1655 37.3585 51.34 36.2511 50.9311 35.303C50.5222 34.3548 49.6072 33.7454 48.5994 33.75H48.5773C51.5155 31.5 52.7208 27.5639 51.5673 23.986C50.4138 20.4081 47.1615 17.995 43.4996 18ZM49.4494 39.875C49.4494 40.3582 49.0689 40.75 48.5994 40.75H38.3997C37.9303 40.75 37.5497 40.3582 37.5497 39.875C37.5497 39.3917 37.9303 39 38.3997 39H48.5994C49.0689 39 49.4494 39.3917 49.4494 39.875ZM49.4494 43.375C49.4494 43.8582 49.0689 44.25 48.5994 44.25H38.3997C37.9303 44.25 37.5497 43.8582 37.5497 43.375C37.5497 42.8917 37.9303 42.5 38.3997 42.5H48.5994C49.0689 42.5 49.4494 42.8917 49.4494 43.375ZM49.4494 46.875C49.4494 47.3582 49.0689 47.75 48.5994 47.75H38.3997C37.9303 47.75 37.5497 47.3582 37.5497 46.875C37.5497 46.3917 37.9303 46 38.3997 46H48.5994C49.0689 46 49.4494 46.3917 49.4494 46.875ZM48.5994 51.25H38.3997C37.9303 51.25 37.5497 50.8582 37.5497 50.375C37.5497 49.8917 37.9303 49.5 38.3997 49.5H48.5994C49.0689 49.5 49.4494 49.8917 49.4494 50.375C49.4494 50.8582 49.0689 51.25 48.5994 51.25ZM48.5994 35.5C49.0689 35.5 49.4494 35.8917 49.4494 36.375C49.4494 36.8582 49.0689 37.25 48.5994 37.25H38.3997C37.9303 37.25 37.5497 36.8582 37.5497 36.375C37.5497 35.8917 37.9303 35.5 38.3997 35.5H48.5994ZM43.4996 33.75C39.7441 33.75 36.6998 30.616 36.6998 26.75C36.6998 22.884 39.7441 19.75 43.4996 19.75C47.255 19.75 50.2994 22.884 50.2994 26.75C50.2952 30.6142 47.2532 33.7457 43.4996 33.75Z" fill="#C9AB63"></path>
							<path d="M3.42106 46.7541H3.39981C1.99157 46.7541 0.849974 47.9284 0.849974 49.377C0.849974 50.8257 1.99157 52 3.39981 52H13.5991C15.0074 52 16.149 50.8257 16.149 49.377C16.149 47.9284 15.0074 46.7541 13.5991 46.7541H13.5779C16.5154 44.5025 17.72 40.5681 16.5669 36.9913C15.4139 33.4145 12.1628 31 8.49948 31C4.83617 31 1.58502 33.4145 0.432008 36.9913C-0.721005 40.5681 0.483568 44.5025 3.42106 46.7541ZM13.5991 48.5027C14.0686 48.5027 14.4491 48.8942 14.4491 49.377C14.4491 49.8599 14.0686 50.2514 13.5991 50.2514H3.39981C2.9304 50.2514 2.54986 49.8599 2.54986 49.377C2.54986 48.8942 2.9304 48.5027 3.39981 48.5027H13.5991ZM8.49948 32.765C12.2548 32.765 15.299 35.8965 15.299 39.7595C15.299 43.6225 12.2548 46.7541 8.49948 46.7541C4.74418 46.7541 1.69992 43.6225 1.69992 39.7595C1.70413 35.8983 4.74593 32.7693 8.49948 32.765Z" fill="#C9AB63"></path>
						</svg>
					</div>
					<div class="interest-step-body">
						<p class="interest-step-description">
							Optimal conditions<br>to Maximize your Trading
						</p>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="sect-divider"></div>
	<div class="section sec-faq">
		<div class="wrapper">
			<center>
				<h2>FAQS </h2>
			</center>
			<ul class="faq">
				<li class="q"><img src="images/arrow.png"> Can you invest in stocks working from home?</li>
				<li class="a">Yes, investing in stocks can be done as a work from home job and it’s actually recommended. The more you save money on hiring employees or paying rent to an office, the more money you will have to invest in stocks. The fact is, you don’t need to have an office or employees in order to invest in the stock market, All you need to have is a working computer a stable internet connection, a comfortable chair and corner where you can work from home at, and the starting capital which is usually around a minimum of £250, and you can begin buying stocks as an online job like any other. </li>

				<li class="q"><img src="images/arrow.png"> How to make money investing in stocks?</li>
				<li class="a">We get asked the question “how to make money by investing in stocks?” all the time, and there is no one answer to this question. In order to make money trading stocks, you have to set your financial goals first, and decide the level of risk you want to take. How is that related to making money buying stocks? Well, that’s what we’re here for, to explain not just how to get started in the markets, but also how to understand things like risk assessment and proper financial goals planning. We will guide you through all investment options and show you which stocks we’re currently looking into. We will show you how to use an online platform to buy stocks with and assist you with your first steps until you are ready to begin your journey on making money buying stocks. </li>

				<li class="q"><img src="images/arrow.png"> Is it possible to earn money online buying shares?</li>
				<li class="a">You could earn money online when buying shares. In fact, there are countless people who earn money online doing just that. When it comes to buying shares, the idea is to buy a share of a company which you believe is going to do well and sell it when you have achieved the return you were hoping for. The difference between the price you bought the share for and the price you sell it for, is the potential profit or loss you could make, and this is how you could potentially earn money buying shares. </li>

				<li class="q"><img src="images/arrow.png"> Can buying shares be a full time work online?</li>
				<li class="a">If you want to work online and buy shares as an online job, the good news is that you can definitely view your work online as a full-time job. In fact, your online job of buying and selling shares will be a fulfilling one too. Why? Because you’ll be in a position to work where you want, how you want, when you want. This is part of the package of having an online job. <br />Many people just like you are working online looking to buy shares which have dropped in value but have the potential to rise. As part of your work online regime, you will also be looking at holding onto shares, even if they have doubled in price since you bought them. This is one of the key elements to having a successful investment portfolio.</li>

				<li class="q"><img src="images/arrow.png"> How do you time your investment so that you can earn in the stock exchange?</li>
				<li class="a">The answer is – you can’t. Usually, the people who think they can find the right time to invest in a stock, end up losing, or not investing at all. This is because, none of us are prophets – none of us can predict the market changes, however, if you decided you want to invest, one recommended strategy is to spread your investment into smaller amounts (for example, instead of making a $10K investment at once, put a small amount of £250 every some time in a stock). If the stock you’ve invested at falls, you will get to buy also when the price goes down, averaging your loss over time, and if it rises, you will still be enjoying its rise while minimizing your risk as much as possible. </li>

				<li class="q"><img src="images/arrow.png"> How do you withdraw the money you earn?</li>
				<li class="a">When you earn money with an online platform buying and selling shares, you could also withdraw any earnings you made. The online platforms we recommend have a seamless and regulated withdrawals process which makes it easy for you to withdraw your money. The entire process - from you looking to earn money, to then having to withdraw the money you have earned, is simple and transparent. </li>

				<li class="q"><img src="images/arrow.png"> Could you create a second income investing in stocks?</li>
				<li class="a">If you’re working in a 9-5 job and you’re looking for a second income, investing in shares is something you could consider. Why? Because if you’re looking for a second income after work, you need to build something which does not take up too much of your time. With investing in stocks, the investment of time you’re going to need is up to you – of course, you could be spending 10 hours a day researching companies, listening to financial news and analyzing stock performance, but it doesn’t necessarily mean you will do better. In fact, a person doing that might fall into the psychological trap of over buying and selling stocks, just because he feels that he needs to “do” something. Having a normal job and buying and investing in stocks as a second income could prove beneficial as you will feel more confident having a regular job and have a stable income you could later invest in buying stocks. </li>
			</ul>
		</div>
	</div>
	<footer>
		<div class="col-md-12">
			<div class="wrapper">
				<div class="footer-logo">
					<img class="footer-img" src="images/logo.png">
				</div>
				<div class="footer-text">
					<p class="interest-footer-text">
						HIGH RISK INVESTMENT WARNING: Investments are complex instruments and come with a high risk of losing money rapidly due to leverage. 82% of retail investor accounts lose money when trading Investments with this provider. You should consider whether you understand how Investments work and whether you can afford to take the high risk of losing your money. Click here to read our Risks Disclosure. Phoenix Markets is the trading name of WGM Services Ltd, a financial services company subject to the regulation and supervision of the Cyprus Securities Exchange Commission (CySEC) under license number 203/13. The offices of WGM Services Ltd are located at 11, Vizantiou, 4th Floor, Strovolos 2064, Nicosia, Cyprus.<br>
						The information and services contained on this website are not intended for residents of the United States or Canada.
					</p>
				</div>
				<div class="col-12 interest-footer-links">
					<a class="interest-footer-link" target="_blank" href="https://www.phoenixmarkets.com/doc/regulation/policies/privacy_policy.pdf">
						Privacy Policy
					</a>
					<a class="interest-footer-link" target="_blank" href="https://www.phoenixmarkets.com/doc/regulation/policies/terms_and_conditions.pdf">
						Terms and Conditions
					</a>
					<a class="interest-footer-link" target="_blank" href="https://www.phoenixmarkets.com/doc/regulation/policies/cookies_policy.pdf">
						Cookie Policy
					</a>
				</div>
			</div>
		</div>
	</footer>

	<div id="reg-form" style="display:none;">
		<div class="container">
			<div class="row d-flex align-items-center form-wrap">
				<div class="col-md-6 reg-left">
					<h2 id="sign-up-page-title">Start investing in Stocks</h2>
					<ul class="phoenix-item">
						<li class="icon-list-item d-flex">
							<div class="col-md-3 icon-list-icon">
								<svg viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M37.5 50C37.2917 50 37.0833 49.9586 36.8875 49.8757C36.4042 49.6706 25 44.7323 25 34.5759V26.2527C25 25.5878 25.425 24.9974 26.0604 24.782L36.9979 21.0823C37.3229 20.9726 37.6771 20.9726 38.0021 21.0823L48.9396 24.782C49.575 24.9974 50 25.5878 50 26.2527V34.5759C50 44.7323 38.5958 49.6706 38.1125 49.8778C37.9167 49.9586 37.7083 50 37.5 50ZM28.125 27.3651V34.5738C28.125 41.478 35.325 45.6292 37.5 46.7229C39.675 45.6292 46.875 41.478 46.875 34.5738V27.3651L37.5 24.1937L28.125 27.3651Z" fill="#C9AB63"></path>
									<path d="M36.9232 41C36.4967 41 36.0831 40.8298 35.7815 40.52L31.4734 36.1563C30.8422 35.517 30.8422 34.4806 31.4734 33.8414C32.1045 33.2021 33.1277 33.2021 33.7589 33.8414L36.7509 36.8719L42.093 29.6544C42.623 28.9322 43.6397 28.7838 44.3548 29.3293C45.0679 29.8704 45.2122 30.8959 44.6758 31.6202L38.2135 40.3476C37.9335 40.7273 37.5048 40.9629 37.0352 40.9978C36.9986 40.9978 36.962 41 36.9232 41Z" fill="#C9AB63"></path>
									<path d="M22.8029 44H6.66177C3.53853 44 1 41.4166 1 38.2381V5.76191C1 2.58343 3.53853 0 6.66177 0H30.3382C33.4615 0 36 2.58343 36 5.76191V17.139C36 18.0065 35.3082 18.7105 34.4559 18.7105C33.6035 18.7105 32.9118 18.0065 32.9118 17.139V5.76191C32.9118 4.31829 31.7568 3.14286 30.3382 3.14286H6.66177C5.24324 3.14286 4.08824 4.31829 4.08824 5.76191V38.2381C4.08824 39.6817 5.24324 40.8571 6.66177 40.8571H22.8029C23.6553 40.8571 24.3471 41.5611 24.3471 42.4286C24.3471 43.296 23.6553 44 22.8029 44Z" fill="#C9AB63"></path>
									<path d="M27.4318 20H7.56818C6.70255 20 6 19.328 6 18.5C6 17.672 6.70255 17 7.56818 17H27.4318C28.2975 17 29 17.672 29 18.5C29 19.328 28.2975 20 27.4318 20Z" fill="#C9AB63"></path>
									<path d="M20.3929 28H8.60714C7.72 28 7 27.328 7 26.5C7 25.672 7.72 25 8.60714 25H20.3929C21.28 25 22 25.672 22 26.5C22 27.328 21.28 28 20.3929 28Z" fill="#C9AB63"></path>
									<path d="M17.375 11H7.625C6.728 11 6 10.328 6 9.5C6 8.672 6.728 8 7.625 8H17.375C18.272 8 19 8.672 19 9.5C19 10.328 18.272 11 17.375 11Z" fill="#C9AB63"></path>
								</svg>
							</div>
							<div class="col-md-9 icon-list-txt">
								Balance protection & enhanced insurance
							</div>
						</li>
						<li class="icon-list-item d-flex">
							<div class="col-md-3 icon-list-icon">
								<img class="gif-imgs" src="images/2.gif" alt="Safe, secure &amp; regulated trading environment">
							</div>
							<div class="col-md-9 pt-3 icon-list-txt">
								Safe, secure & regulated trading environment
							</div>
						</li>
						<li class="icon-list-item d-flex">
							<div class="col-md-3 icon-list-icon">
								<svg viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M2.59161 27.001C3.28141 27.0021 3.94327 26.7358 4.43102 26.261C4.49863 26.1952 8.6594 22.1526 8.6594 22.1526L10.7944 24.2325C11.6073 25.0245 12.7103 25.4695 13.8604 25.4695C15.0105 25.4695 16.1134 25.0245 16.9263 24.2325L29.6444 11.8484L33.5122 15.6151C35.2066 17.2616 37.9506 17.2616 39.645 15.6151L47.3875 8.06833L48.0307 8.69441C48.6035 9.25142 49.4571 9.43274 50.2162 9.15868C50.9753 8.88461 51.501 8.20534 51.5621 7.41947L51.9955 1.98471V1.97627C52.0331 1.44344 51.8321 0.920832 51.4443 0.543016C51.0565 0.1652 50.5198 -0.0309002 49.9724 0.00521119L44.3839 0.427099C43.5768 0.483287 42.8778 0.993012 42.5954 1.73116C42.3131 2.4693 42.4996 3.29993 43.0724 3.8562L43.7156 4.48397L36.5773 11.4358L32.7104 7.66923C31.8975 6.87728 30.7945 6.43231 29.6444 6.43231C28.4943 6.43231 27.3914 6.87728 26.5784 7.66923L13.8604 20.0542L11.7271 17.9726C10.0329 16.3256 7.28846 16.3256 5.5943 17.9726L0.761737 22.6893C0.0190585 23.4106 -0.204013 24.4964 0.196565 25.4403C0.597144 26.3842 1.54246 27.0002 2.59161 27.001ZM1.98483 23.879L6.82086 19.1615C7.30862 18.6861 7.97051 18.419 8.6607 18.419C9.35089 18.419 10.0128 18.6861 10.5005 19.1615L13.2475 21.8371C13.4101 21.9955 13.6307 22.0845 13.8608 22.0845C14.0909 22.0845 14.3115 21.9955 14.4741 21.8371L27.8041 8.85979C28.8337 7.90212 30.4551 7.90212 31.4847 8.85979L35.3525 12.6264C36.0386 13.2643 37.1186 13.2643 37.8047 12.6264L45.555 5.07799C45.8928 4.74859 45.8928 4.21513 45.555 3.88574L44.2955 2.65973C44.2003 2.57369 44.1702 2.439 44.2201 2.32222C44.2638 2.19816 44.3811 2.1128 44.5157 2.10706L50.1007 1.6877C50.1443 1.68578 50.1867 1.70241 50.2168 1.73326C50.2511 1.76528 50.2695 1.81024 50.2671 1.85646L49.8337 7.29037C49.8242 7.41909 49.7385 7.53061 49.6143 7.57581C49.4901 7.621 49.3503 7.59157 49.2564 7.50047L48.0003 6.27615C47.6562 5.95952 47.1179 5.95952 46.7738 6.27615L38.4184 14.4211C37.3881 15.3761 35.7691 15.3761 34.7388 14.4211L30.871 10.6545C30.5465 10.3365 30.1048 10.1582 29.6444 10.1592C29.1848 10.1587 28.7439 10.3367 28.4196 10.6537L15.7015 23.0394C14.6717 23.9967 13.0507 23.9967 12.021 23.0394L9.88596 20.9595C9.20906 20.3013 8.11233 20.3013 7.43544 20.9595L3.20272 25.0729C2.86309 25.3965 2.32012 25.3965 1.9805 25.0729C1.81654 24.9179 1.72403 24.7048 1.72403 24.4823C1.72403 24.2598 1.81654 24.0467 1.9805 23.8917L1.98483 23.879Z" fill="#C9AB63"></path>
									<path d="M34 32.7618C34.0075 28.6441 31.2217 25.079 27.3113 24.2017C23.4008 23.3243 19.4253 25.3726 17.7658 29.1196C16.1062 32.8667 17.2124 37.2972 20.4218 39.7575H20.4005C19.3928 39.7529 18.4778 40.362 18.0689 41.3096C17.6599 42.2571 17.8345 43.3638 18.5136 44.1298C17.6296 45.1184 17.6296 46.6391 18.5136 47.6277C17.8345 48.3936 17.6599 49.5004 18.0689 50.4479C18.4778 51.3955 19.3928 52.0046 20.4005 52H30.6001C31.6079 52.0046 32.5228 51.3955 32.9318 50.4479C33.3407 49.5004 33.1662 48.3936 32.487 47.6277C33.3711 46.6391 33.3711 45.1184 32.487 44.1298C33.1662 43.3638 33.3407 42.2571 32.9318 41.3096C32.5228 40.362 31.6079 39.7529 30.6001 39.7575H30.5789C32.729 38.1123 33.9974 35.5187 34 32.7618ZM31.4501 45.8787C31.4501 46.3617 31.0695 46.7532 30.6001 46.7532H20.4005C19.9311 46.7532 19.5506 46.3617 19.5506 45.8787C19.5506 45.3958 19.9311 45.0043 20.4005 45.0043H30.6001C31.0695 45.0043 31.4501 45.3958 31.4501 45.8787ZM30.6001 50.2511H20.4005C19.9311 50.2511 19.5506 49.8595 19.5506 49.3766C19.5506 48.8936 19.9311 48.5021 20.4005 48.5021H30.6001C31.0695 48.5021 31.4501 48.8936 31.4501 49.3766C31.4501 49.8595 31.0695 50.2511 30.6001 50.2511ZM30.6001 41.5064C31.0695 41.5064 31.4501 41.8979 31.4501 42.3809C31.4501 42.8639 31.0695 43.2554 30.6001 43.2554H20.4005C19.9311 43.2554 19.5506 42.8639 19.5506 42.3809C19.5506 41.8979 19.9311 41.5064 20.4005 41.5064H30.6001ZM18.7006 32.7618C18.7006 28.8982 21.7449 25.7661 25.5003 25.7661C29.2557 25.7661 32.3001 28.8982 32.3001 32.7618C32.3001 36.6254 29.2557 39.7575 25.5003 39.7575C21.7467 39.7532 18.7048 36.6236 18.7006 32.7618Z" fill="#C9AB63"></path>
									<path fill-rule="evenodd" clip-rule="evenodd" d="M25.4992 31.75C25.0948 31.7496 24.7491 31.4881 24.6792 31.1298C24.6092 30.7714 24.8352 30.4195 25.2152 30.2951C25.5952 30.1708 26.0195 30.3099 26.2217 30.625C26.3656 30.8657 26.6436 31.0169 26.9473 31.0196C27.2511 31.0222 27.5323 30.876 27.6815 30.6379C27.8306 30.3997 27.824 30.1075 27.6642 29.875C27.3621 29.4187 26.8904 29.0734 26.3325 28.9V28.75C26.3325 28.3358 25.9594 28 25.4992 28C25.0389 28 24.6658 28.3358 24.6658 28.75V28.888C23.5187 29.2516 22.8302 30.3069 23.0363 31.3856C23.2424 32.4644 24.2829 33.2521 25.4992 33.25C25.9035 33.2504 26.2492 33.5119 26.3191 33.8702C26.3891 34.2286 26.1631 34.5805 25.7831 34.7049C25.4031 34.8292 24.9788 34.6901 24.7767 34.375C24.5397 34.0302 24.0412 33.9174 23.6511 34.1202C23.2611 34.323 23.1207 34.768 23.3341 35.125C23.6367 35.5815 24.109 35.927 24.6675 36.1V36.25C24.6675 36.6642 25.0406 37 25.5008 37C25.9611 37 26.3342 36.6642 26.3342 36.25V36.112C27.4816 35.7483 28.1701 34.6926 27.9636 33.6136C27.757 32.5347 26.7157 31.7472 25.4992 31.75Z" fill="#C9AB63"></path>
									<path fill-rule="evenodd" clip-rule="evenodd" d="M8.49916 38.75C8.09485 38.7496 7.74915 38.4881 7.6792 38.1298C7.60925 37.7714 7.83523 37.4195 8.21521 37.2951C8.59518 37.1708 9.01951 37.3099 9.22167 37.625C9.3656 37.8657 9.64364 38.0169 9.94734 38.0196C10.2511 38.0222 10.5323 37.876 10.6815 37.6379C10.8306 37.3997 10.824 37.1075 10.6642 36.875C10.3621 36.4187 9.89036 36.0734 9.33251 35.9V35.75C9.33251 35.3358 8.9594 35 8.49916 35C8.03892 35 7.66582 35.3358 7.66582 35.75V35.888C6.51868 36.2516 5.83021 37.3069 6.03631 38.3856C6.2424 39.4644 7.28295 40.2521 8.49916 40.25C8.90348 40.2504 9.24918 40.5119 9.31913 40.8702C9.38908 41.2286 9.16309 41.5805 8.78312 41.7049C8.40314 41.8292 7.97881 41.6901 7.77665 41.375C7.53966 41.0302 7.04115 40.9174 6.6511 41.1202C6.26106 41.323 6.12067 41.768 6.33413 42.125C6.63666 42.5815 7.10902 42.927 7.66748 43.1V43.25C7.66748 43.6642 8.04059 44 8.50083 44C8.96107 44 9.33417 43.6642 9.33417 43.25V43.112C10.4816 42.7483 11.1701 41.6926 10.9636 40.6136C10.757 39.5347 9.71569 38.7472 8.49916 38.75Z" fill="#C9AB63"></path>
									<path fill-rule="evenodd" clip-rule="evenodd" d="M43.4992 25.75C43.0948 25.7496 42.7491 25.4881 42.6792 25.1298C42.6092 24.7714 42.8352 24.4195 43.2152 24.2951C43.5952 24.1708 44.0195 24.3099 44.2217 24.625C44.3656 24.8657 44.6436 25.0169 44.9473 25.0196C45.2511 25.0222 45.5323 24.876 45.6815 24.6379C45.8306 24.3997 45.824 24.1075 45.6642 23.875C45.3621 23.4187 44.8904 23.0734 44.3325 22.9V22.75C44.3325 22.3358 43.9594 22 43.4992 22C43.0389 22 42.6658 22.3358 42.6658 22.75V22.888C41.5187 23.2516 40.8302 24.3069 41.0363 25.3856C41.2424 26.4644 42.2829 27.2521 43.4992 27.25C43.9035 27.2504 44.2492 27.5119 44.3191 27.8702C44.3891 28.2286 44.1631 28.5805 43.7831 28.7049C43.4031 28.8292 42.9788 28.6901 42.7767 28.375C42.5397 28.0302 42.0412 27.9174 41.6511 28.1202C41.2611 28.323 41.1207 28.768 41.3341 29.125C41.6367 29.5815 42.109 29.927 42.6675 30.1V30.25C42.6675 30.6642 43.0406 31 43.5008 31C43.9611 31 44.3342 30.6642 44.3342 30.25V30.112C45.4816 29.7483 46.1701 28.6926 45.9636 27.6136C45.757 26.5347 44.7157 25.7472 43.4992 25.75Z" fill="#C9AB63"></path>
									<path d="M43.4996 18C39.838 17.9958 36.5864 20.4088 35.4329 23.9862C34.2793 27.5636 35.4838 31.4993 38.421 33.75H38.3997C37.392 33.7454 36.477 34.3548 36.068 35.303C35.6591 36.2511 35.8336 37.3585 36.5128 38.125C35.6287 39.1142 35.6287 40.6358 36.5128 41.625C35.6287 42.6142 35.6287 44.1358 36.5128 45.125C35.6287 46.1142 35.6287 47.6358 36.5128 48.625C35.8336 49.3914 35.6591 50.4988 36.068 51.447C36.477 52.3951 37.392 53.0046 38.3997 53H48.5994C49.6072 53.0046 50.5222 52.3951 50.9311 51.447C51.34 50.4988 51.1655 49.3914 50.4864 48.625C51.3704 47.6358 51.3704 46.1142 50.4864 45.125C51.3704 44.1358 51.3704 42.6142 50.4864 41.625C51.3704 40.6358 51.3704 39.1142 50.4864 38.125C51.1655 37.3585 51.34 36.2511 50.9311 35.303C50.5222 34.3548 49.6072 33.7454 48.5994 33.75H48.5773C51.5155 31.5 52.7208 27.5639 51.5673 23.986C50.4138 20.4081 47.1615 17.995 43.4996 18ZM49.4494 39.875C49.4494 40.3582 49.0689 40.75 48.5994 40.75H38.3997C37.9303 40.75 37.5497 40.3582 37.5497 39.875C37.5497 39.3917 37.9303 39 38.3997 39H48.5994C49.0689 39 49.4494 39.3917 49.4494 39.875ZM49.4494 43.375C49.4494 43.8582 49.0689 44.25 48.5994 44.25H38.3997C37.9303 44.25 37.5497 43.8582 37.5497 43.375C37.5497 42.8917 37.9303 42.5 38.3997 42.5H48.5994C49.0689 42.5 49.4494 42.8917 49.4494 43.375ZM49.4494 46.875C49.4494 47.3582 49.0689 47.75 48.5994 47.75H38.3997C37.9303 47.75 37.5497 47.3582 37.5497 46.875C37.5497 46.3917 37.9303 46 38.3997 46H48.5994C49.0689 46 49.4494 46.3917 49.4494 46.875ZM48.5994 51.25H38.3997C37.9303 51.25 37.5497 50.8582 37.5497 50.375C37.5497 49.8917 37.9303 49.5 38.3997 49.5H48.5994C49.0689 49.5 49.4494 49.8917 49.4494 50.375C49.4494 50.8582 49.0689 51.25 48.5994 51.25ZM48.5994 35.5C49.0689 35.5 49.4494 35.8917 49.4494 36.375C49.4494 36.8582 49.0689 37.25 48.5994 37.25H38.3997C37.9303 37.25 37.5497 36.8582 37.5497 36.375C37.5497 35.8917 37.9303 35.5 38.3997 35.5H48.5994ZM43.4996 33.75C39.7441 33.75 36.6998 30.616 36.6998 26.75C36.6998 22.884 39.7441 19.75 43.4996 19.75C47.255 19.75 50.2994 22.884 50.2994 26.75C50.2952 30.6142 47.2532 33.7457 43.4996 33.75Z" fill="#C9AB63"></path>
									<path d="M3.42106 46.7541H3.39981C1.99157 46.7541 0.849974 47.9284 0.849974 49.377C0.849974 50.8257 1.99157 52 3.39981 52H13.5991C15.0074 52 16.149 50.8257 16.149 49.377C16.149 47.9284 15.0074 46.7541 13.5991 46.7541H13.5779C16.5154 44.5025 17.72 40.5681 16.5669 36.9913C15.4139 33.4145 12.1628 31 8.49948 31C4.83617 31 1.58502 33.4145 0.432008 36.9913C-0.721005 40.5681 0.483568 44.5025 3.42106 46.7541ZM13.5991 48.5027C14.0686 48.5027 14.4491 48.8942 14.4491 49.377C14.4491 49.8599 14.0686 50.2514 13.5991 50.2514H3.39981C2.9304 50.2514 2.54986 49.8599 2.54986 49.377C2.54986 48.8942 2.9304 48.5027 3.39981 48.5027H13.5991ZM8.49948 32.765C12.2548 32.765 15.299 35.8965 15.299 39.7595C15.299 43.6225 12.2548 46.7541 8.49948 46.7541C4.74418 46.7541 1.69992 43.6225 1.69992 39.7595C1.70413 35.8983 4.74593 32.7693 8.49948 32.765Z" fill="#C9AB63"></path>
								</svg>
							</div>
							<div class="col-md-9 icon-list-txt">
								Optimal conditions to Maximize your Trading
							</div>
						</li>
					</ul>
					<div class="bottom-text">By signing up you will receive marketing emails, which you may unsubscribe from anytime.</div>
					<div class="bottom-text">All trading involves risk. Only risk capital you're prepared to lose.</div>
				</div>

				<div class="col-md-6 reg-right">
					<div class="form-wrapper">
						<h2 class="sign-up-title">Your Opportunity to Invest in Amazon Shares</h2>
						<form action="form.php" method="post" onsubmit="return Validation()" class="interest-form ">
							<?php if (isset($_GET['clickid']) && ($_GET['clickid'] != "")) { ?>
								<input type="hidden" value="<?php echo $_GET['clickid']; ?>" name="clickid" />
							<?php  } ?>
							<input type="hidden" value="" name="title" id="title" />
							<div class="input-wrapper">
								<input class="input-field" id="fname" name="FirstNameLastName" placeholder="Full Name " type="text" required="" onkeyup="checkName();">
								<div id="fname-message" class="error-msg"></div>
							</div>
							<div class="input-wrapper">
								<input class="input-field " id="email" name="email" placeholder="Email " type="email" required="" onkeyup="checkEmail();">
								<div id="email-message" class="error-msg"></div>
							</div>
							<div class="input-wrapper">
								<input class="input-field " id="password" name="password" placeholder="Password " type="password" required="" onkeyup="checkPassword();">
								<div id="password-message" class="error-msg"></div>
							</div>
							<div class="input-wrapper" id="phone-input-wrapper">
								<input class="input-field " style="width: 100%;" type="tel" name="phone" id="phone" class="phone form-control input-fields" placeholder="Phone Number" required="true" value="">
								<div id="phone-message" class="error-msg"></div>
							</div>
							<label class="custom-checkbox-label" for="custom-checkbox-input">
								<input type="checkbox" id="custom-checkbox-input" class="custom-checkbox-input" checked="checked">
								<span class="checkmark"></span>
								<span class="conditions">I accept the Terms &amp; Conditions</span>
								<span class="age">I confirm that I am 18 or older </span>
							</label>
							<button id="commence-btn" class="button-trading">Start Trading Now</button>
						</form>

					</div>
				</div>

			</div>
		</div>
	</div>


</body>

<script>
	$(window).scroll(function() {
		if ($(this).scrollTop() > 600) {
			$("#top").addClass('down');
		} else {
			$("#top").removeClass('down');
		}
	});
</script>


<script>
	const phoneInputField = document.querySelector("#phone");
	const phoneInput = window.intlTelInput(phoneInputField, {
		initialCountry: "auto",
		geoIpLookup: function(success) {
			// Get your api-key at https://ipdata.co/
			fetch("https://api.ipdata.co/?api-key=fb6483095ab6d6ef96d67904a2d8fe9e05c462b3d7063c54f5024f89")
				.then(function(response) {
					if (!response.ok) return success("");
					return response.json();
				})
				.then(function(ipdata) {
					success(ipdata.country_code);
				});
		},
		utilsScript: "https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js?1613236686837", // just for formatting/placeholders etc
	});

	$('#commence-btn').click(function() {
		var code = phoneInput.getNumber();
		$('#phone').val(code);
		var title = $("title").text();
		$('#title').val(title);
	});
</script>

<script>
	var checkName = function() {
		var regExp = new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{2,})");
		var fname = document.getElementById("fname").value;
		if ((fname.length < 2) || (fname.search(/[0-9]/) > 0) || (!regExp.test(fname) && fname) || (fname.split(" ").length > 2)) {
			document.getElementById("fname-message").innerHTML = "Name should be followed by First name , Space and then Last name.";
			return false;
		}
		if (fname.length > 32) {
			document.getElementById("fname-message").innerHTML = "The length of the name should not exceed 32 characters.";
			return false;
		}
		document.getElementById("fname-message").innerHTML = " ";
		return true;
	}
</script>

<script>
	$('#email').first().keyup(function() {
		var $email = this.value;
		validateEmail($email);
	});

	function validateEmail(email) {
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		if ((email.length < 2) || (!emailReg.test(email))) {
			document.getElementById("email-message").innerHTML = "This field is required, please enter a valid email address";
		} else {
			document.getElementById("email-message").innerHTML = " ";
			return true;
		}
	}
</script>

<script>
	$('#phone').first().keyup(function() {
		var phone = this.value;
		if (phone.length < 5) {
			document.getElementById("phone-message").innerHTML = "This field is required";
		} else {
			document.getElementById("phone-message").innerHTML = " ";
			return true;
		}
	});
</script>

<script>
	var checkPassword = function() {
		var pw = document.getElementById("password").value;
		if ((pw.length < 8) || (pw.search(/[a-z]/) < 0) || (pw.search(/[A-Z]/) < 0) || (pw.search(/[0-9]/) < 0)) {
			document.getElementById("password-message").innerHTML = "The password length must be at least 8 characters. The password must contain at least one letter, one capital letter and one number";
			return false;
		}
		if (pw.length > 15) {
			document.getElementById("password-message").innerHTML = "La lunghezza della password non deve superare i 15 caratteri.";
			return false;
		}
		document.getElementById("password-message").innerHTML = " ";
		return true;
	}
</script>

<script>
	function Validation() {
		var regExp = new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{2,})");
		var fname = document.getElementById("fname").value;
		if ((fname.length < 2) || (fname.search(/[0-9]/) > 0) || (!regExp.test(fname) && fname) || (fname.split(" ").length > 2)) {
			document.getElementById("fname-message").innerHTML = "Please enter only your first and last name";
			return false;
		}
		if (fname.length > 32) {
			document.getElementById("fname-message").innerHTML = "The length of the name must not exceed 32 characters.";
			return false;
		}
		var pw = document.getElementById("password").value;
		if ((pw.length < 8) || (pw.search(/[a-z]/) < 0) || (pw.search(/[A-Z]/) < 0) || (pw.search(/[0-9]/) < 0)) {
			document.getElementById("password-message").innerHTML = "The password length must be at least 8 characters. The password must contain at least one letter, one capital letter and one number";
			return false;
		}
		if (pw.length > 15) {
			document.getElementById("password-message").innerHTML = "La lunghezza della password non deve superare i 15 caratteri.";
			return false;
		}
		var phone = document.getElementById("phone").value;
		if (phone.length < 5) {
			document.getElementById("phone-message").innerHTML = "This field is required";
			return false;
		} 
		var email = document.getElementById("email").value;
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		if ((email.length < 2) || (!emailReg.test(email))) {
			document.getElementById("email-message").innerHTML = "This field is required, please enter a valid email address";
		    return false;
		}	    
	}
</script>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script>
	// Accordian Action
	var action = 'click';
	var speed = "500";


	$(document).ready(function() {

		// Question handler
		$('li.q').on(action, function() {

			// gets next element
			// opens .a of selected question
			$(this).next().slideToggle(speed)

				// selects all other answers and slides up any open answer
				.siblings('li.a').slideUp();

			// Grab img from clicked question
			var img = $(this).children('img');

			// remove Rotate class from all images except the active
			$('img').not(img).removeClass('rotate');

			// toggle rotate class
			img.toggleClass('rotate');

		});

	});
</script>

<script >
	//popup register
	document.addEventListener('DOMContentLoaded', () => {
		let users = [
			{
				"name": "Allan",
				"country": "English",
				"ip": "55.17.200.128"
			},
			{
				"name": "Daniel",
				"country": "English",
				"ip": "189.190.23.118"
			},
			{
				"name": "Richard",
				"country": "English",
				"ip": "76.42.50.69"
			},
			{
				"name": "Achim",
				"country": "Germany",
				"ip": "57.173.172.78"
			},
			{
				"name": "Calixto Campos",
				"country": "Spain",
				"ip": "62.169.207.139"
			},
			{
				"name": "Santino",
				"country": "Italy",
				"ip": "80.11.241.200"
			},
			{
				"name": "Maximilien",
				"country": "France",
				"ip": "166.13.37.52"
			},
			{
				"name": "Steven Ekdal",
				"country": "Sweden",
				"ip": "95.29.52.105"
			},
			{
				"name": "Conrad",
				"country": "Germany",
				"ip": "85.207.106.58"
			},
			{
				"name": "Lazaro Herrera",
				"country": "Spain",
				"ip": "68.5.176.147"
			},
		];

		let elem = document.createElement('div');
		elem.className = 'popup-register';
		document.body.appendChild(elem);
		let show = true;
		let indexUser = 0;
		let timerId;

		function startRegisterPopup() {
			timerId = setInterval(() => {
				if (show) {
					if (indexUser == users.length) indexUser = 0;
					let name = users[indexUser].name;
					let country = users[indexUser].country;
					let ip = users[indexUser].ip;
					if (ip != '') { ip = ip.split('.')[3]; } else ip = "X";
					document.querySelector('.popup-register').innerHTML = '<div class="user-info"><div class="name">'
						+ name + ' from <img src="./images/reg-popup/'
						+ country + '.jpg"> has registered <span class="time-ago">'
						+ getRandomIntInclusive(1, 5) + ' minutes ago</span></div><button class="btn-register">Register</button><div class="verified"><img class="tick_blue" src="./images/reg-popup/tick_blue.png"> Verified Registrant (IP: X.X.X.'
						+ ip + ')</div></div>';
					elem.classList.add("show");
					show = false;
					indexUser++;
				} else {
					elem.classList.remove("show");
					show = true;
				}
			}, 5000);
		};

		elem.onmouseover = function () {
			clearInterval(timerId);
			elem.style = "margin-bottom:10px; ";
			show = false;
		};

		elem.onmouseout = function () {
			elem.style = "margin-bottom:0px;";
			startRegisterPopup();
		};

		startRegisterPopup();
	});

	function getRandomIntInclusive(min, max) {
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
</script>

<script>		
	$(document).on('click', 'button', function() {
		$('#top').addClass('form-head');
		$('.sect-divider').hide();
		$('footer').hide();
		$('section').hide();
		$('.section').hide();
		$('#reg-form').show();
		$('#close-btn').show();
	});
	$(".link-scroll").click(function() {
		$('#top').addClass('form-head');
		$('.sect-divider').hide();
		$('footer').hide();
		$('section').hide();
		$('.section').hide();
		$('#reg-form').show();
		$('#close-btn').show();
	});

	$("#close-btn").click(function() {
		$('#top').removeClass('form-head');
		$('.sect-divider').show();
		$('footer').show();
		$('section').show();
		$('.section').show();
		$('#reg-form').hide();
		$('#close-btn').hide();
	});
</script>


</html>